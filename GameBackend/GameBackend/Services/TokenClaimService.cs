using GameBackend.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Newtonsoft.Json;


namespace GameBackend.Services
{
    public class TokenClaimService : ITokenClaimService
    {
        private readonly IMongoDatabase _database;

        public TokenClaimService(IOptions<GameBackendDatabaseSettings> gameBackendDatabaseSettings)
        {
            var mongoClient = new MongoClient(gameBackendDatabaseSettings.Value.ConnectionString);
            _database = mongoClient.GetDatabase(gameBackendDatabaseSettings.Value.DatabaseName);
        }

        // Method to get the collection by name
        private IMongoCollection<Player> GetCollectionByName(string collectionName)
        {
            return _database.GetCollection<Player>(collectionName);
        }

        public async Task<Player?> GetPlayerByUserId(string collectionName, string userId)
        {
            var collection = GetCollectionByName(collectionName);
            return await collection.Find(x => x.UserId == userId).FirstOrDefaultAsync();
        }

        public int GetTokensRequestedAmount(TokensReq? tokensReqRef)
        {
            return tokensReqRef?.TokensReqValue ?? 0;
        }

        public int GetTokensClaimed(TokensClaim? tokensClaimRef)
        {
            return tokensClaimRef?.TokensClaimed ?? 0;
        }

        public int GetTotalScore(Score scoreRef)
        {
            return scoreRef.TotalScore;
        }

        public async Task<ClaimStatusResult> CheckClaimStatus(string collectionName, string userId, string? autosignerUrl = null)
        {
            var player = await GetPlayerByUserId(collectionName, userId);

            if (player == null)
            {
                throw new ArgumentException("Player not found.");
            }

            var tokensReqRef = player.TokensReqData;
            var tokensClaimRef = player.TokensClaimData;
            var scoreRef = player.ScoreData;

            var tokensRequestedAmount = GetTokensRequestedAmount(tokensReqRef);
            var tokensClaimed = GetTokensClaimed(tokensClaimRef);
            var totalScore = GetTotalScore(scoreRef);

            if (tokensRequestedAmount == 0 || string.IsNullOrEmpty(tokensReqRef?.Txnhash))
            {
                // Handle the case when tokensRequestedAmount is 0 or Txnhash is null or empty

                return new ClaimStatusResult { Success = false, Message = "Invalid tokens requested amount or Txnhash." };
            }

            AutosignerToken secureData = null;

            if (!string.IsNullOrEmpty(autosignerUrl))
            {
                if (autosignerUrl == "https://xar-autosigner-2.proximaxtest.com")
                {
                    var collection = _database.GetCollection<AutosignerToken>("~ metx-secure");
                    var node = "testnet";
                    secureData = await collection.Find(x => x.Node == node).FirstOrDefaultAsync();

                    if (secureData == null || string.IsNullOrEmpty(secureData.BearerToken))
                    {
                        throw new ArgumentException("Bearer token not found in the database.");
                    }
                }
                else if (autosignerUrl == "https://xar-autosigner.proximaxtest.com")
                {
                    var collection = _database.GetCollection<AutosignerToken>("~ metx-secure");
                    var node = "mainnet";
                    secureData = await collection.Find(x => x.Node == node).FirstOrDefaultAsync();

                    if (secureData == null || string.IsNullOrEmpty(secureData.BearerToken))
                    {
                        throw new ArgumentException("Bearer token not found in the database.");
                    }
                }
                else
                {
                    throw new ArgumentException("Autosigner Url not found.");
                }
                // Make a request to autosignerUrl to get the response
                using var httpClient = new HttpClient();
                string apiUrl = $"{autosignerUrl}/api/v1/transactions/{player.TokensReqData.Txnhash}";
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", secureData.BearerToken);

                var response = await httpClient.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    var now = DateTime.UtcNow;
                    var time = now.ToString("yyyy-MM-dd HH:mm:ss");
                    var autosignerResponseJson = await response.Content.ReadAsStringAsync();
                    var autosignerResponse = JsonConvert.DeserializeObject<AutosignerResponse>(autosignerResponseJson);

                    // Update player data based on the autosigner response
                    if (autosignerResponse?.Success == true && autosignerResponse.ViewModel?.Status == "Success")
                    {
                        // Post data to the HistoryClaim collection
                        var historyClaimCollection = _database.GetCollection<HistoryClaim>("HistoryClaim");
                        var historyClaim = new HistoryClaim
                        {
                            ClaimId = Guid.NewGuid().ToString(),
                            UserId = player.UserId,
                            GameID = player.UserData.GameID,
                            Tokens = tokensRequestedAmount,
                            Txnhash = player.TokensReqData.Txnhash,
                            Time = time
                        };
                        await historyClaimCollection.InsertOneAsync(historyClaim);

                        tokensClaimed += tokensRequestedAmount;
                        player.TokensReqData.TokensReqValue = 0;
                        player.TokensReqData.Txnhash = "empty";
                        player.TokensReqData.TRUpdated = now;
                        scoreRef.DailyScore = 0;
                        scoreRef.DSUpdated = now;
                        player.TokensClaimData.TokensClaimed = tokensClaimed;
                        player.TokensClaimData.TCUpdated = now;

                        // Update the player document in the database
                        var collection = GetCollectionByName(collectionName);
                        await collection.ReplaceOneAsync(x => x.UserId == player.UserId, player);

                        return new ClaimStatusResult
                        {
                            Success = true,
                            Message = "Claim status checked and handled",
                            Data = new
                            {
                                txnStatus = "Success",
                                time = time,
                            },
                        };
                    }
                    else if (autosignerResponse?.Success == false)
                    {
 
                        // Handle case where autosigner response indicates failure
                        totalScore += tokensRequestedAmount;

                        scoreRef.TotalScore = totalScore;
                        scoreRef.TSUpdated = now;
                        player.TokensReqData.TokensReqValue = 0;
                        player.TokensReqData.Txnhash = "empty";
                        player.TokensReqData.TRUpdated = now;

                        // Update the player document in the database
                        var collection = GetCollectionByName(collectionName);
                        await collection.ReplaceOneAsync(x => x.UserId == player.UserId, player);

                        if (autosignerResponse.Message.Contains("Transaction does not exist, or is not yet signed by recepient"))
                        {
                            return new ClaimStatusResult
                            {
                                Success = false,
                                Message = "Transaction failed: Transaction does not exist or is not yet signed by recipient.",
                                Data = new
                                {
                                    txnStatus = "Fail",
                                    time = time,
                                    totalScore = totalScore,
                                },
                            };
                        }
                        else
                        {
                            return new ClaimStatusResult
                            {
                                Success = false,
                                Message = "Transaction failed",
                                Data = new
                                {
                                    txnStatus = "Fail",
                                    time = time,
                                    totalScore = totalScore,
                                },
                            };
                        }
                    }
                    else
                    {
                        // Handle other autosigner response status values here if needed
                        return new ClaimStatusResult { Success = false, Message = "Unexpected autosigner response." };
                    }
                }
            }
            return new ClaimStatusResult { Success = false, Message = "Invalid hash or unexpected error." };
        }

        /// <summary>
        ///  only update data when txn succedded
        public async Task<ClaimStatusResult> UpdatePlayerDataOnSuccess(string collectionName, string userId)
        {
            try
            {
                var player = await GetPlayerByUserId(collectionName, userId);

                if (player == null)
                {
                    throw new ArgumentException("Player not found.");
                }

                var tokensReqRef = player.TokensReqData;
                var tokensClaimRef = player.TokensClaimData;
                var scoreRef = player.ScoreData;

                var tokensRequestedAmount = GetTokensRequestedAmount(tokensReqRef);
                var tokensClaimed = GetTokensClaimed(tokensClaimRef);
                var totalScore = GetTotalScore(scoreRef);

                var now = DateTime.UtcNow;
                var time = now.ToString("yyyy-MM-dd HH:mm:ss");

                if (tokensRequestedAmount == 0 || string.IsNullOrEmpty(tokensReqRef?.Txnhash))
                {
                    // Handle the case when tokensRequestedAmount is 0 or Txnhash is null or empty
                    return new ClaimStatusResult { Success = false, Message = "Invalid tokens requested amount or Txnhash." };
                }

                var historyClaimCollection = _database.GetCollection<HistoryClaim>("HistoryClaim");
                var historyClaim = new HistoryClaim
                {
                    ClaimId = Guid.NewGuid().ToString(),
                    UserId = player.UserId,
                    GameID = player.UserData.GameID,
                    Tokens = tokensRequestedAmount,
                    Txnhash = player.TokensReqData.Txnhash,
                    Time = time
                };
                await historyClaimCollection.InsertOneAsync(historyClaim);

                tokensClaimed += tokensRequestedAmount;
                player.TokensReqData.TokensReqValue = 0;
                player.TokensReqData.Txnhash = "empty";
                player.TokensReqData.TRUpdated = now;
                scoreRef.DailyScore = 0;
                scoreRef.DSUpdated = now;
                player.TokensClaimData.TokensClaimed = tokensClaimed;
                player.TokensClaimData.TCUpdated = now;

                // Update the player document in the database
                var collection = GetCollectionByName(collectionName);
                await collection.ReplaceOneAsync(x => x.UserId == player.UserId, player);

                return new ClaimStatusResult
                {
                    Success = true,
                    Message = "Claim status checked and handled",
                    Data = new
                    {
                        txnStatus = "Success",
                        time = time,
                    },
                };
            }
            catch (ArgumentException ex)
            {
                return new ClaimStatusResult { Success = false, Message = ex.Message };
            }
            catch (Exception ex)
            {
                return new ClaimStatusResult { Success = false, Message = "An error occurred while updating player data on failure." };
            }
        }


        /// <summary>
        /// Called when getting error 500 or only update data when txn failed
        public async Task<ClaimStatusResult> UpdatePlayerDataOnFailure(string collectionName, string userId)
        {
            try
            {
                var player = await GetPlayerByUserId(collectionName, userId);

                if (player == null)
                {
                    throw new ArgumentException("Player not found.");
                }

                var scoreRef = player.ScoreData;

                var now = DateTime.UtcNow;
                var time = now.ToString("yyyy-MM-dd HH:mm:ss");

                var tokensRequestedAmount = GetTokensRequestedAmount(player.TokensReqData);

                // Handle case where autosigner response indicates failure
                var totalScore = GetTotalScore(scoreRef) + tokensRequestedAmount;

                scoreRef.TotalScore = totalScore;
                scoreRef.TSUpdated = now;
                player.TokensReqData.TokensReqValue = 0;
                player.TokensReqData.Txnhash = "empty";
                player.TokensReqData.TRUpdated = now;

                // Update the player document in the database
                var collection = GetCollectionByName(collectionName);
                await collection.ReplaceOneAsync(x => x.UserId == player.UserId, player);

                return new ClaimStatusResult
                {
                    Success = false,
                    Message = "Transaction failed",
                    Data = new
                    {
                        txnStatus = "Fail",
                        time = time,
                        totalScore = totalScore,
                    },
                };
            }
            catch (ArgumentException ex)
            {
                return new ClaimStatusResult { Success = false, Message = ex.Message };
            }
            catch (Exception ex)
            {
                return new ClaimStatusResult { Success = false, Message = "An error occurred while updating player data on failure." };
            }
        }
    }
}



