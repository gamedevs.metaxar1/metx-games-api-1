using GameBackend.Models;
using System.Threading.Tasks;

namespace GameBackend.Services
{
    public interface IScoreService
    {
        Task<ScoreUpdateResult> UpdateScore(string collectionName, string userId, int score, int? limit = null);
    }
}
