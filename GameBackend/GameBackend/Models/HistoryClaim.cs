using System;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GameBackend.Models
{
    public class HistoryClaim
    {
        [BsonId]
        [BsonElement("claimId")]
        [JsonPropertyName("claimId")]
        public string? ClaimId { get; set; } = null;

        [BsonElement("userId")]
        [JsonPropertyName("userId")]
        public string? UserId { get; set; } = null;

        [BsonElement("gameId")]
        [JsonPropertyName("gameId")]
        public string? GameID { get; set; }

        [BsonElement("tokens")]
        [JsonPropertyName("tokens")]
        public int Tokens { get; set; }

        [BsonElement("txn_hash")]
        [JsonPropertyName("txn_hash")]
        public string? Txnhash { get; set; } = null!;

        [BsonElement("time")]
        [JsonPropertyName("time")]
        public string? Time { get; set; } = null!;
    }
}