using System;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GameBackend.Models
{
    public class AutosignerToken
    {
        [BsonId]
        [BsonElement("node")]
        [JsonPropertyName("node")]
        public string? Node { get; set; } = null;

        [BsonElement("bearerToken")]
        [JsonPropertyName("bearerToken")]
        public string? BearerToken { get; set; } = null;
    }
}