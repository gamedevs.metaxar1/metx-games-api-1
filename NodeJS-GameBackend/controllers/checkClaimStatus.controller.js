const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');

const checkClaimStatus = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { autosigner_url, eventID } = req.query;

    let ref;
    if (eventID !== undefined && eventID !== null && eventID !== "") {
      ref = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}`);
    } else {
      ref = admin.database().ref(`${gameID}/${user}`);
    }
    const tokensReqRef = ref.child('c_TokensReq');
    const tokensClaimRef = ref.child('d_TokensClaim');
    const scoreRef = ref.child('b_Score');

    // Fetch bearer token from Firebase Realtime Database
    const metxSecureRef = admin.database().ref(`~ metx-secure`);
    const bearerTokenSnapshot = await metxSecureRef.child('bearerToken').once('value');
    const bearerToken = bearerTokenSnapshot.val();

    const tokensReqSnapshot = await tokensReqRef.once('value');
    const tokensClaimSnapshot = await tokensClaimRef.once('value');
    const scoreSnapshot = await scoreRef.once('value');

    const tokensReqVal = tokensReqSnapshot.val();

    if (!tokensReqVal) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for token request.",
        data: {},
      });
    }

    let tokensRequestedAmount = tokensReqVal.a_TokensReq;
    let tokensClaimedVal = tokensClaimSnapshot.val();
    let tokensClaimed = tokensClaimedVal ? tokensClaimedVal.g_TokensClaimed : 0;
    let totalScore = scoreSnapshot.val().d_TotalScore;
    const t1 = moment(tokensReqVal.c_TR_Updated).unix()- 28800;
    const t2 = moment().tz(timezone).unix();
    const duration = 3600;

    // Ensure tokensRequestedAmount, tokensClaimed and totalScore are numbers, if not default them to 0
    if (typeof tokensRequestedAmount !== 'number') tokensRequestedAmount = 0;
    if (typeof tokensClaimed !== 'number') tokensClaimed = 0;
    if (typeof totalScore !== 'number') totalScore = 0;

    const hash = tokensReqVal.b_TxnHash;

    // If there is no token request made or no transaction hash, return a message to user and do nothing
    if (!tokensRequestedAmount || !hash || tokensRequestedAmount == 0) {
      return res.status(200).json({
        success: false,
        message: "There is no token request made or no transaction hash available.",
        data: {},
      });
    }

    // Call the autosigner API to get the transaction status
    const response = await axios.get(`${autosigner_url}/api/v1/transactions/${hash}`, {
      headers: { Authorization: `Bearer ${bearerToken}` },
    });

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss');

    // Check transaction status and perform updates accordingly
    const transaction = response.data.viewModel;
    let tokensClaimedThisTime = 0;  // Track tokens claimed in this request

    // Check if transaction is successful
    if (transaction.group === 'confirmed' && transaction.status === 'Success') {
      tokensClaimedThisTime = tokensRequestedAmount;  // Tokens claimed is equal to the tokens requested
      tokensClaimed += tokensClaimedThisTime;  // Update total tokens claimed

      // reset daily score, reset tokens requested, update tokens claimed
      await Promise.all([
        tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
        scoreRef.update({ a_DailyScore: 0, b_DS_Updated: time}),
        tokensClaimRef.update({ g_TokensClaimed: tokensClaimed, h_TC_Updated: time}),
      ]);

      const viewModel = {
        success: true,
        message: "Claim status checked and handled",
        data: {
          txnStatus: transaction.status,
          time,
          totalScore,
          tokensRequestedAmount,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      };
      postHistory(gameID, eventID, user, tokensClaimedThisTime, hash);
      return res.status(200).json(viewModel);


    } else if (transaction.status === 'Fail') {
      // Transaction failed but within 5 minutes, so we don't update tokensClaimed or totalScore, and don't reset tokensReq
      // Do nothing
      return res.status(400).json({
        success: false,
        message: 'Transation not completed'
      })
    } else if (transaction.status !== 'Success' && t2 - t1 > duration) {
      // after 5 mins and txn status still not success: retrieve data from tokensreq to total score, tokensreq reset, daily no change
      totalScore += tokensRequestedAmount;

      await Promise.all([
        scoreRef.update({ d_TotalScore: totalScore, e_TS_Updated: time }),
        tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
      ]);

      return res.status(200).json({
        success: false,
        message: "Transaction failed after 1 hour. The requested tokens have been returned to your score.",
        duration: t2 - t1,
        data: {
          txnStatus: transaction.status,
          time,
          totalScore,
          tokensRequestedAmount: 0,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      });
    } else {
      return res.status(200).json({
        success: false,
        message: "Please scan QR your previous claim",
        duration: t2 - t1,
        data: {
          txnStatus: transaction.status,
          time,
          totalScore,
          tokensRequestedAmount,
          tokensClaimed,
          tokensClaimedThisTime,
        },
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

const updateTokenClaimSuccess = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { eventID } = req.query;
    let ref;
    if (eventID !== undefined && eventID !== null && eventID !== "") {
      ref = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}`);
    } else {
      ref = admin.database().ref(`${gameID}/${user}`);
    }
    const tokensReqRef = ref.child('c_TokensReq');
    const tokensClaimRef = ref.child('d_TokensClaim');
    const scoreRef = ref.child('b_Score');

    // Retrieve the tokensRequested amount, tokensClaimed, and totalScore from the database
    const tokensReqSnapshot = await tokensReqRef.once('value');
    const tokensClaimSnapshot = await tokensClaimRef.once('value');
    const scoreSnapshot = await scoreRef.once('value');

    const tokensReqVal = tokensReqSnapshot.val();

    if (!tokensReqVal) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for token request.",
        data: {
          eventID,
          tokensReqVal
        },
      });
    }

    let tokensRequestedAmount = tokensReqVal.a_TokensReq;
    let tokensClaimedVal = tokensClaimSnapshot.val();
    let tokensClaimed = tokensClaimedVal ? tokensClaimedVal.g_TokensClaimed : 0;
    let totalScore = scoreSnapshot.val().d_TotalScore;

    // Ensure tokensRequestedAmount, tokensClaimed, and totalScore are numbers, if not default them to 0
    if (typeof tokensRequestedAmount !== 'number') tokensRequestedAmount = 0;
    if (typeof tokensClaimed !== 'number') tokensClaimed = 0;
    if (typeof totalScore !== 'number') totalScore = 0;

    const hash = tokensReqVal.b_TxnHash;

    // If there is no token request made or no transaction hash, return a message to the user and do nothing
    if (!tokensRequestedAmount || !hash || tokensRequestedAmount == 0) {
      return res.status(200).json({
        success: false,
        message: "There is no token request made or no transaction hash available.",
        data: {
          eventID,
          tokensRequestedAmount,
          hash
        },
      });
    }

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss');

    // Simulate success for this example (replace this with your actual logic)
    const transactionStatus = 'Success';

    let tokensClaimedThisTime = 0;  // Track tokens claimed in this request

    // Check if transaction is successful
    if (transactionStatus === 'Success') {
      tokensClaimedThisTime = tokensRequestedAmount;  // Tokens claimed is equal to the tokens requested
      tokensClaimed += tokensClaimedThisTime;  // Update total tokens claimed

      // reset daily score, reset tokens requested, update tokens claimed
      if (tokensRequestedAmount > 0) {
        await Promise.all([
          tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
          scoreRef.update({ a_DailyScore: 0, b_DS_Updated: time}),
          tokensClaimRef.update({ g_TokensClaimed: tokensClaimed, h_TC_Updated: time}),
        ]);
      }

      const viewModel = {
        success: true,
        message: "Tokens Claimed updated successfully",
        data: {
          eventID,
          txnStatus: transactionStatus,
          time,
          totalScore,
          tokensRequestedAmount: 0,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      };
      postHistory(gameID, eventID, user, tokensClaimedThisTime, hash);

      return res.status(200).json(viewModel);

    } else {
      return res.status(400).json({
        success: false,
        message: "Tokens Claimed NOT updated.",
        data: {
          eventID,
          txnStatus: transactionStatus,
          time,
          totalScore,
          tokensRequestedAmount,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

const updateTokenClaimFail = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { eventID } = req.query;
    let ref;
    if (eventID !== undefined && eventID !== null && eventID !== "") {
      ref = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}`);
    } else {
      ref = admin.database().ref(`${gameID}/${user}`);
    }
    const tokensReqRef = ref.child('c_TokensReq');
    const scoreRef = ref.child('b_Score');

    // Retrieve the tokensRequested amount and totalScore from the database
    const tokensReqSnapshot = await tokensReqRef.once('value');
    const scoreSnapshot = await scoreRef.once('value');

    const tokensReqVal = tokensReqSnapshot.val();

    if (!tokensReqVal || tokensReqVal == 0) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for token request.",
        data: {
          eventID,
          tokensReqVal
        },
      });
    }

    let tokensRequestedAmount = tokensReqVal.a_TokensReq;
    let totalScore = scoreSnapshot.val().d_TotalScore;

    // Ensure tokensRequestedAmount and totalScore are numbers, if not default them to 0
    if (typeof tokensRequestedAmount !== 'number') tokensRequestedAmount = 0;
    if (typeof totalScore !== 'number') totalScore = 0;

    // Add tokensRequestedAmount to the totalScore
    totalScore += tokensRequestedAmount;

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss');

    // Update the totalScore and reset tokens requested
    if (tokensRequestedAmount > 0)
    {
      await Promise.all([
        scoreRef.update({ d_TotalScore: totalScore, e_TS_Updated: time }),
        tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
      ]);
    }

    const viewModel = {
      success: false,
      message: "Tokens Claimed NOT updated. Tokens requested added to total score.",
      data: {
        txnStatus: "Fail",
        time,
        totalScore,
        tokensRequestedAmount: 0,
      },
    };

    return res.status(200).json(viewModel);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

const updateTokenClaimFailbyDuration = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { eventID } = req.query;
    let ref;
    if (eventID !== undefined && eventID !== null && eventID !== "") {
      ref = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}`);
    } else {
      ref = admin.database().ref(`${gameID}/${user}`);
    }
    const tokensReqRef = ref.child('c_TokensReq');
    const scoreRef = ref.child('b_Score');

    // Retrieve the tokensRequested amount and totalScore from the database
    const tokensReqSnapshot = await tokensReqRef.once('value');
    const scoreSnapshot = await scoreRef.once('value');

    const tokensReqVal = tokensReqSnapshot.val();

    if (!tokensReqVal || tokensReqVal == 0) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for token request.",
        data: {
          eventID,
          tokensReqVal
        },
      });
    }

    let tokensRequestedAmount = tokensReqVal.a_TokensReq;
    let totalScore = scoreSnapshot.val().d_TotalScore;

    // Ensure tokensRequestedAmount and totalScore are numbers, if not default them to 0
    if (typeof tokensRequestedAmount !== 'number') tokensRequestedAmount = 0;
    if (typeof totalScore !== 'number') totalScore = 0;

    const now = moment().tz(timezone);
    const t2 = moment().tz(timezone).unix();
    const t1 = moment(tokensReqVal.c_TR_Updated, "YYYY-MM-DD HH:mm:ss", "Europe/London").unix() - 28800;
    const duration = 3600;
    const time = now.format('YYYY-MM-DD HH:mm:ss');
    var timeOutMsg = "Please scan QR your previous claim.";

    // Update the totalScore and reset tokens requested
    if (tokensRequestedAmount > 0 && t2 - t1 > duration)
    {
      // Add tokensRequestedAmount to the totalScore
      totalScore += tokensRequestedAmount;
      tokensRequestedAmount = 0;
      timeOutMsg = "Tokens Claimed NOT updated. Tokens requested added to total score."

      await Promise.all([
        scoreRef.update({ d_TotalScore: totalScore, e_TS_Updated: time }),
        tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
      ]);
    }

    const viewModel = {
      success: false,
      message: timeOutMsg,
      data: {
        eventID,
        txnStatus: "Fail",
        t2,
        t1,
        duration: t2 - t1,
        timeRequest: tokensReqVal.c_TR_Updated,
        time,
        totalScore,
        tokensRequestedAmount,
      },
    };

    return res.status(200).json(viewModel);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

function postHistory(gameID, eventID, user, amount, hash) {
  const now = moment().tz(timezone);
  const time = now.format('YYYY-MM-DD HH:mm:ss');
  const ref = admin.database().ref(`${gameID}/${user}/d_TokensClaim/z_History`);
  const refEvent = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}/d_TokensClaim/z_History`);
  const historyData = {
    [time]: {
      eventID: eventID,
      tokens: amount,
      txnhash: hash,
    },
  };
  ref.update(historyData);
  refEvent.update(historyData);
};

const updateHighScoreTokenClaim = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { eventID } = req.query;
    let ref;
    if (eventID !== undefined && eventID !== null && eventID !== "") {
      ref = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}`);
    } else {
      ref = admin.database().ref(`${gameID}/${user}`);
    }
    const tokensReqRef = ref.child('c_TokensReq');
    const tokensClaimRef = ref.child('d_TokensClaim');
    const highScoreRef = ref.child('g_HighScore');

    const tokensReqSnapshot = await tokensReqRef.once('value');
    const tokensClaimSnapshot = await tokensClaimRef.once('value');
    const highScoreSnapshot = await highScoreRef.once('value');

    const tokensReqVal = tokensReqSnapshot.val();

    if (!tokensReqVal) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for token request.",
        data: {
          eventID,
          tokensReqVal
        },
      });
    }

    let tokensRequestedAmount = tokensReqVal.a_TokensReq;
    let tokensClaimedVal = tokensClaimSnapshot.val();
    let tokensClaimed = tokensClaimedVal ? tokensClaimedVal.g_TokensClaimed : 0;
    let highScore;
    if (eventID !== undefined && eventID !== null && eventID !== "") {
      highScore = highScoreSnapshot.child(eventID).val()
    } else {
      highScore = highScoreSnapshot.child('HighScore').val();
    }

    if (typeof tokensRequestedAmount !== 'number') tokensRequestedAmount = 0;
    if (typeof tokensClaimed !== 'number') tokensClaimed = 0;
    if (typeof highScore !== 'number') highScore = 0;

    const hash = tokensReqVal.b_TxnHash;

    // If there is no token request made or no transaction hash, return a message to the user and do nothing
    if (!tokensRequestedAmount || tokensRequestedAmount == 0) {
      return res.status(200).json({
        success: false,
        message: "There is no token request made or no transaction hash available.",
        data: {
          eventID,
          tokensRequestedAmount,
          hash
        },
      });
    }

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss');

    // Simulate success for this example (replace this with your actual logic)
    const transactionStatus = 'Success';

    let tokensClaimedThisTime = 0;  // Track tokens claimed in this request

    // Check if transaction is successful
    if (transactionStatus === 'Success') {
      tokensClaimedThisTime = tokensRequestedAmount;  // Tokens claimed is equal to the tokens requested
      tokensClaimed += tokensClaimedThisTime;  // Update total tokens claimed

      // reset daily score, reset tokens requested, update tokens claimed
      if (tokensRequestedAmount > 0) {
        await Promise.all([
          tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
          tokensClaimRef.update({ g_TokensClaimed: tokensClaimed, h_TC_Updated: time}),
        ]);
      }

      const viewModel = {
        success: true,
        message: "Tokens Claimed updated successfully",
        data: {
          eventID,
          txnStatus: transactionStatus,
          time,
          highScore,
          tokensRequestedAmount: 0,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      };
      postHistory(gameID, eventID, user, tokensClaimedThisTime, hash);

      return res.status(200).json(viewModel);

    } else {
      return res.status(400).json({
        success: false,
        message: "Tokens Claimed NOT updated.",
        data: {
          eventID,
          txnStatus: transactionStatus,
          time,
          highScore,
          tokensRequestedAmount,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

module.exports = {
  checkClaimStatus,
  updateTokenClaimSuccess,
  updateTokenClaimFail,
  updateTokenClaimFailbyDuration,
  updateHighScoreTokenClaim
};
