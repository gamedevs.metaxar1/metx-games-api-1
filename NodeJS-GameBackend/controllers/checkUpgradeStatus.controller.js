const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');

const checkUpgradeStatus = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { autosigner_url } = req.query;

    // Fetch historical data upgrade
    const snapshotUpgrade = admin.database().ref(`${gameID}/${user}/e_Upgrade`);
    const ref = await snapshotUpgrade.once('value');
    let dailyUpgradePoint = ref.child('a_DailyUpgrade');
    let dailyUpgradeStr = dailyUpgradePoint.val();
    let dailyUpgrade = Number(dailyUpgradeStr);

    let monthlyUpgradePoint = ref.child('c_MonthlyUpgrade');
    let monthlyUpgradeStr = monthlyUpgradePoint.val();
    let monthlyUpgrade = Number(monthlyUpgradeStr);

    let totalUpgradePoint = ref.child('e_TotalUpgrade');
    let totalUpgradeStr = totalUpgradePoint.val();
    let totalUpgrade = Number(totalUpgradeStr);

    //Fetch upgrade request database
    const snapshotReq = admin.database().ref(`${gameID}/${user}/f_UpgradesReq`);
    const upgradeRef = await snapshotReq.once('value');
    let upgradeReqPoint = upgradeRef.child('a_UpgradesReq');
    let upgradeReqStr = upgradeReqPoint.val(); // This would be "90"
    let upgradeReq = Number(upgradeReqStr);

    let txnHash = upgradeRef.child('b_TxnHash').val();
    let txnTime = upgradeRef.child('c_UR_Updated');

    // Fetch bearer token from Firebase Realtime Database
    const metxSecureRef = admin.database().ref(`~ metx-secure`);
    const bearerTokenSnapshot = await metxSecureRef.child('bearerToken').once('value');
    const bearerToken = bearerTokenSnapshot.val();

    if (!upgradeReq) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for upgrade request.",
        data: {},
      });
    }

    // Ensure daily, monthly, total upgrade are numbers, if not default them to 0
    if (typeof dailyUpgrade !== 'number') dailyUpgrade = 0;
    if (typeof monthlyUpgrade !== 'number') monthlyUpgrade = 0;
    if (typeof totalUpgrade !== 'number') totalUpgrade = 0;

    // If there is no token request made or no transaction hash, return a message to user and do nothing
    if (!upgradeReq || !txnHash || upgradeReq == 0 || !autosigner_url) {
      return res.status(400).json({
        success: false,
        message: "There is no upgrade request made or no transaction hash available or no autosigner available.",
        data: {},
      });
    }

    // Call the autosigner API to get the transaction status
    const response = await axios.get(`${autosigner_url}/api/v1/transactions/${txnHash}`, {
      headers: { Authorization: `Bearer ${bearerToken}` },
    });

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 

    // Check transaction status and perform updates accordingly
    const transaction = response.data.viewModel;
    let tokensClaimedThisTime = 0;  // Track tokens claimed in this request

    // Check if transaction is successful
    if (transaction.group === 'confirmed' && transaction.status === 'Success') {
      dailyUpgrade += upgradeReq;
      totalUpgrade += upgradeReq;
      monthlyUpgrade += upgradeReq;

      // reset daily score, reset tokens requested, update tokens claimed
      await Promise.all([
        snapshotUpgrade.update({  a_DailyUpgrade: dailyUpgrade, 
                                  b_DU_Updated: time,
                                  c_MonthlyUpgrade: monthlyUpgrade, 
                                  d_MU_Updated: time,
                                  e_TotalUpgrade: totalUpgrade, 
                                  f_TU_Updated: time  }),

        snapshotReq.update({  a_UpgradesReq: 0, 
                              b_TxnHash: "",
                              c_UR_Updated: time  })
      ]);

      const viewModel = {
        success: true,
        message: "Claim status checked and handled",
        data: {
          dailyUpgrade: dailyUpgrade,
          monthlyUpgrade: monthlyUpgrade,
          totalUpgrade: totalUpgrade,
          time: time,
          transactionHash: txnHash,
        },
      };
      postHistory(gameID, user, tokensClaimedThisTime, txnHash);
      return res.status(200).json(viewModel);
 
      
    } else if (transaction.status === 'Fail') { 
      // Transaction failed but within 5 minutes, so we don't update tokensClaimed or totalScore, and don't reset tokensReq
      // Do nothing
      return res.status(400).json({
        success: false,
        message: 'Transation not completed'
      })
    } else if (transaction.status !== 'Success' && now.diff(moment(transaction.deadline), 'minutes') > 5) { 
      // after 5 mins and txn status still not success: retrieve data from tokensreq to total score, tokensreq reset, daily no change

      await Promise.all([
        snapshotReq.update({  a_UpgradesReq: 0, 
                              b_TxnHash: null,
                              c_UR_Updated: time  })
                          ]);

      return res.status(200).json({
        success: false,
        message: "Transaction failed after 5 minutes. Please make a new transaction",
        data: {
          dailyUpgrade: dailyUpgrade,
          monthlyUpgrade: monthlyUpgrade,
          totalUpgrade: totalUpgrade,
          time: time,
        },
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

const putUpgradeSuccess = async (req, res) => {
  try {
    const { gameID, user } = req.params;

    // Fetch historical data upgrade
    const snapshotUpgrade = admin.database().ref(`${gameID}/${user}/e_Upgrade`);
    const ref = await snapshotUpgrade.once('value');
    let dailyUpgradePoint = ref.child('a_DailyUpgrade');
    let dailyUpgradeStr = dailyUpgradePoint.val();
    let dailyUpgrade = Number(dailyUpgradeStr);

    let monthlyUpgradePoint = ref.child('c_MonthlyUpgrade');
    let monthlyUpgradeStr = monthlyUpgradePoint.val();
    let monthlyUpgrade = Number(monthlyUpgradeStr);

    let totalUpgradePoint = ref.child('e_TotalUpgrade');
    let totalUpgradeStr = totalUpgradePoint.val();
    let totalUpgrade = Number(totalUpgradeStr);

    //Fetch upgrade request database
    const snapshotReq = admin.database().ref(`${gameID}/${user}/f_UpgradesReq`);
    const upgradeRef = await snapshotReq.once('value');
    let upgradeReqPoint = upgradeRef.child('a_UpgradesReq');
    let upgradeReqStr = upgradeReqPoint.val(); // This would be "90"
    let upgradeReq = Number(upgradeReqStr);

    let txnHash = upgradeRef.child('b_TxnHash');
    let txnTime = upgradeRef.child('c_UR_Updated');

    if (!upgradeReq) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for upgrade request.",
        data: {},
      });
    }

    // Ensure daily, monthly, total upgrade are numbers, if not default them to 0
    if (typeof dailyUpgrade !== 'number') dailyUpgrade = 0;
    if (typeof monthlyUpgrade !== 'number') monthlyUpgrade = 0;
    if (typeof totalUpgrade !== 'number') totalUpgrade = 0;

    // If there is no token request made or no transaction hash, return a message to user and do nothing
    if (!upgradeReq || !txnHash || upgradeReq == 0) {
      return res.status(400).json({
        success: false,
        message: "There is no upgrade request made or no transaction hash available or no autosigner available.",
        data: {},
      });
    }

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 

    // Check if transaction is successful
    if (upgradeReq > 0) {
      dailyUpgrade += upgradeReq;
      totalUpgrade += upgradeReq;
      monthlyUpgrade += upgradeReq;

      // reset daily score, reset tokens requested, update tokens claimed
      await Promise.all([
        snapshotUpgrade.update({  a_DailyUpgrade: dailyUpgrade, 
                                  b_DU_Updated: time,
                                  c_MonthlyUpgrade: monthlyUpgrade, 
                                  d_MU_Updated: time,
                                  e_TotalUpgrade: totalUpgrade, 
                                  f_TU_Updated: time  }),

        snapshotReq.update({  a_UpgradesReq: 0, 
                              b_TxnHash: "",
                              c_UR_Updated: time  })
      ]);

      const viewModel = {
        success: true,
        message: "Claim status checked and handled",
        data: {
          dailyUpgrade: dailyUpgrade,
          monthlyUpgrade: monthlyUpgrade,
          totalUpgrade: totalUpgrade,
          time: time,
          transactionHash: txnHash,
        },
      };
      // postHistory(gameID, user, upgradeReq, hash);
      return res.status(200).json(viewModel);
    } 
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

const putUpgradeFail = async (req, res) => {
  try {
    const { gameID, user } = req.params;

    // Fetch historical data upgrade
    const snapshotUpgrade = admin.database().ref(`${gameID}/${user}/e_Upgrade`);
    const ref = await snapshotUpgrade.once('value');
    let dailyUpgradePoint = ref.child('a_DailyUpgrade');
    let dailyUpgradeStr = dailyUpgradePoint.val();
    let dailyUpgrade = Number(dailyUpgradeStr);

    let monthlyUpgradePoint = ref.child('c_MonthlyUpgrade');
    let monthlyUpgradeStr = monthlyUpgradePoint.val();
    let monthlyUpgrade = Number(monthlyUpgradeStr);

    let totalUpgradePoint = ref.child('e_TotalUpgrade');
    let totalUpgradeStr = totalUpgradePoint.val();
    let totalUpgrade = Number(totalUpgradeStr);

    //Fetch upgrade request database
    const snapshotReq = admin.database().ref(`${gameID}/${user}/f_UpgradesReq`);
    const upgradeRef = await snapshotReq.once('value');
    let upgradeReqPoint = upgradeRef.child('a_UpgradesReq');
    let upgradeReqStr = upgradeReqPoint.val(); // This would be "90"
    let upgradeReq = Number(upgradeReqStr);

    let txnHashPoint = upgradeRef.child('b_TxnHash');
    let txnHash = txnHashPoint.val();
    let txnTime = upgradeRef.child('c_UR_Updated');

    if (!upgradeReq) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for upgrade request.",
        data: {},
      });
    }

    // Ensure daily, monthly, total upgrade are numbers, if not default them to 0
    if (typeof dailyUpgrade !== 'number') dailyUpgrade = 0;
    if (typeof monthlyUpgrade !== 'number') monthlyUpgrade = 0;
    if (typeof totalUpgrade !== 'number') totalUpgrade = 0;

    // If there is no token request made or no transaction hash, return a message to user and do nothing
    if (!upgradeReq || !txnHash || upgradeReq == 0 || !autosigner_url) {
      return res.status(400).json({
        success: false,
        message: "There is no upgrade request made or no transaction hash available or no autosigner available.",
        data: {},
      });
    }

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 

    // Check if transaction is successful
    if (upgradeReq > 0) {

      // reset daily score, reset tokens requested, update tokens claimed
      await Promise.all([
        snapshotUpgrade.update({  a_DailyUpgrade: 0, 
                                  b_DU_Updated: time,
                                  c_MonthlyUpgrade: 0, 
                                  d_MU_Updated: time,
                                  e_TotalUpgrade: 0, 
                                  f_TU_Updated: time  }),

        snapshotReq.update({  a_UpgradesReq: 0, 
                              b_TxnHash: "",
                              c_UR_Updated: time  })
      ]);

      const viewModel = {
        success: true,
        message: "All upgrade request reset to zero.",
        data: {
          dailyUpgrade: dailyUpgrade,
          monthlyUpgrade: monthlyUpgrade,
          totalUpgrade: totalUpgrade,
          time: time,
          transactionHash: txnHash,
        },
      };
      // postHistory(gameID, user, upgradeReq);
      return res.status(200).json(viewModel);
    } 
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

function postHistory(gameID, user, amount, hash) {
  const now = moment().tz(timezone);
  const time = now.format('YYYY-MM-DD HH:mm:ss'); 
  const ref = admin.database().ref(`${gameID}/${user}/f_UpgradesReq/z_History`);
  const historyData = {
    [time]: {
      Upgrades: amount,
      TxnHash: hash,
    },
  };
  ref.update(historyData);
}

module.exports = {
  checkUpgradeStatus,
  putUpgradeSuccess,
  putUpgradeFail
};
